"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_data_collector admin *

:details: lara_django_data_collector admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev tables_generator lara_django_data_collector >> tables.py" to update this file
________________________________________________________________________
"""
