"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_data_collector admin *

:details: lara_django_data_collector admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev forms_generator -c lara_django_data_collector > forms.py" to update this file
________________________________________________________________________
"""
